import logging
from typing import Dict

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.api.users.routes import router as users_router
from src.api.auth.routes import router as auth_router
from src.api.protected_routes.routes import router as protected_router
from src.api.verification_code.routes import router as verification_code_router

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

app = FastAPI()
app.include_router(auth_router)
app.include_router(users_router)
app.include_router(protected_router)
app.include_router(verification_code_router)


# Configure CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:3000"
    ],  # Adjust the list of allowed origins if needed
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root() -> Dict:
    return {"Hello": "World"}
