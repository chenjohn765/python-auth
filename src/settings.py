from pydantic import BaseSettings


class Settings(BaseSettings):
    DB_USERNAME: str
    DB_PASSWORD: str
    DB_NAME: str
    DB_HOST: str
    DB_PORT: str
    GOOGLE_CLIENT_ID: str
    EMAIL: str
    EMAIL_PASSWORD: str


settings = Settings()
