import boto3
from src.settings import Settings
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def send_aws_email(recipient_email: str, subject: str, message: str):
    # WARNING: the ECS task running the docker container of this API must
    # have SES policies

    # Set up AWS SES client
    ses_client = boto3.client("ses")

    # Email details
    sender_email = Settings().EMAIL  # Your email address
    sender_password = Settings().EMAIL_PASSWORD  # Your email password

    # Send the email
    # In Sandbox, the SES service only allows to send to verified emails.
    response = ses_client.send_email(
        Source=sender_email,
        Destination={"ToAddresses": [recipient_email]},
        Message={"Subject": {"Data": subject}, "Body": {"Text": {"Data": message}}},
    )
    logger.info(f"Verification code email sent to {sender_email}")
    logger.info("Email sent! Message ID:", response["MessageId"])
