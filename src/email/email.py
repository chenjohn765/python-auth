import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr
from src.settings import Settings
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

# Email configuration


def send_email(recipient: str, subject: str, email_body: str) -> None:
    sender_email = Settings().EMAIL  # Your email address
    sender_password = Settings().EMAIL_PASSWORD  # Your email password
    sender_name = "Jcauth"
    recipient_email = recipient  # Recipient's email address
    subject = subject

    # Create the email content
    message = MIMEMultipart()
    message["From"] = (sender_name, sender_email)
    message["To"] = recipient_email
    message["Subject"] = subject

    # Email body
    message.attach(MIMEText(email_body, "plain"))

    # Connect to the SMTP server
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()

        server.login(sender_email, sender_password)

        # Send the email
        server.sendmail(sender_email, recipient_email, message.as_string())
        logger.info(f"Verification code email sent to {sender_email}")
    except Exception as e:
        logger.error("Error:", e)
    finally:
        server.quit()
