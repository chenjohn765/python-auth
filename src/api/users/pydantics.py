from datetime import datetime

from pydantic import BaseModel

from src.registries.user import UserSource


class UserResponse(BaseModel):
    username: str
    disabled: bool
    created_at: datetime
    source: UserSource

    class Config:
        orm_mode = True
