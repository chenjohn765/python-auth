import logging
from datetime import datetime

from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from src.exceptions.http_exceptions import UsernameAlreadyExistsException
from src.api.users.pydantics import UserResponse
from src.api.utils.depends.depends import DependsSession, get_active_user
from src.models import User
from src.registries.user import UserRegistry, UserRegistryInput, UserSource
from src.registries.verification_code import VerificationCodeRegistry
from fastapi.param_functions import Form

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
router = APIRouter()


class VerificationCodeForm:
    def __init__(self, verification_id: str = Form(), verification_code: int = Form()):
        self.verification_code = verification_code
        self.verification_id = verification_id


@router.get("/users/me", response_model=UserResponse)
def me(user: User = Depends(get_active_user)) -> UserResponse:
    return user


@router.post("/users/register", response_model=UserResponse)
def register(
    session: DependsSession,
    form_data: OAuth2PasswordRequestForm = Depends(),
    verification_code_form: VerificationCodeForm = Depends(),
) -> UserResponse:
    verification_code_registry = VerificationCodeRegistry(session)
    verification_code_registry.validate_code(
        verification_code_form.verification_id, verification_code_form.verification_code
    )

    user_registry = UserRegistry(session)
    user = user_registry.get(form_data.username)

    if user:
        raise UsernameAlreadyExistsException()

    user = user_registry.create(
        UserRegistryInput(
            username=form_data.username,
            password=form_data.password,
            disabled=False,
            created_at=datetime.utcnow(),
            source=UserSource.backend,
        )
    )

    return user
