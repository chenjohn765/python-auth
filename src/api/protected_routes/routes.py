import logging

from fastapi import APIRouter, Depends

from src.api.utils.depends.depends import get_active_user
from src.models import User

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
router = APIRouter()


@router.get("/protected/1")
def protected_one(user: User = Depends(get_active_user)) -> str:
    return "Hello, this is Protected info 1"
