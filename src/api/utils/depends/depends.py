import logging
from typing import Annotated

import jwt
from fastapi import Depends
from sqlalchemy.orm import Session

from src.exceptions.http_exceptions import (
    AuthenticationException,
    DisabledUserException,
    TokenExpiredException,
    UserDoesNotExistException,
)
from src.api.auth.utils import decode_access_token, fetch_user, oauth2_scheme
from src.connect import get_session
from src.models import User

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# Needed, otherwise FastAPI raises an Error
DependsSession = Annotated[Session, Depends(get_session)]


async def get_user(
    session: DependsSession, token: str = Depends(oauth2_scheme)
) -> User:
    try:
        decoded_token = decode_access_token(token)

    except jwt.exceptions.ExpiredSignatureError:
        raise TokenExpiredException()

    except jwt.exceptions.PyJWTError as e:
        logger.error(f"Error when decoding jwt token token {token}. Error is {e}")
        raise AuthenticationException()

    user = fetch_user(session, decoded_token.sub)
    if not user:
        raise UserDoesNotExistException()

    return user


async def get_active_user(user: User = Depends(get_user)) -> User:
    if user.disabled:
        raise DisabledUserException()
    return user
