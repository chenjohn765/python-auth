from datetime import datetime
from enum import Enum

from pydantic import BaseModel
from uuid import UUID


class TokenTypeEnum(Enum):
    bearer = "bearer"


class Token(BaseModel):
    """Payload that the decoded token should respect."""

    sub: str
    exp: datetime


class RefreshToken(BaseModel):
    """Payload that the decoded token should respect."""

    sub: str
    exp: datetime


class TokenResponse(BaseModel):
    """Reponse returned by the API for the /token route"""

    access_token: str
    refresh_token: str
    token_type: TokenTypeEnum


class VerificationCodeSendInput(BaseModel):
    destination: str


class VerificationCodeSendResponse(BaseModel):
    id: UUID


class RefreshTokenInput(BaseModel):
    refresh_token: str


class GoogleLoginInput(BaseModel):
    google_token: str


class GoogleIdInfo(BaseModel):
    # url source https://accounts.google.com/
    iss: str
    # identifier like 102091680547620533731
    sub: str
    iat: int
    exp: int
    email: str
    email_verified: bool
    name: str
