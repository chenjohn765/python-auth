from fastapi import Depends
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import APIRouter


from src.api.auth.utils import (
    create_access_token,
    create_refresh_token,
    decode_refresh_token,
    authenticate_user,
)
from src.exceptions.http_exceptions import (
    AuthenticationException,
    RefreshTokenExpiredException,
    RefreshTokenDisabledException,
    WrongCredentialsException,
    InvalidGoogleToken,
)
from src.api.auth.pydantics import (
    TokenResponse,
    TokenTypeEnum,
    RefreshTokenInput,
    GoogleLoginInput,
    GoogleIdInfo,
)
from src.registries.refresh_token import RefreshTokenRegistry, RefreshTokenRegistryInput
from src.registries.user import UserRegistry, UserRegistryInput, UserSource
from src.api.utils.depends.depends import DependsSession
import jwt
import logging
from datetime import datetime

from google.oauth2 import id_token
from google.auth.transport import requests
from src.settings import settings


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
router = APIRouter()


@router.post("/token", response_model=TokenResponse)
def token(
    session: DependsSession, form_data: OAuth2PasswordRequestForm = Depends()
) -> TokenResponse:
    user = authenticate_user(session, form_data.username, form_data.password)
    if not user:
        raise WrongCredentialsException()

    token = create_access_token(user.username)
    refresh_token = create_refresh_token(user.username)

    # Must save refresh_token in DB

    refresh_token_registry = RefreshTokenRegistry(session)
    refresh_token_registry.invalidate_all_refresh_tokens(user.username)
    refresh_token_registry.create(
        RefreshTokenRegistryInput(
            username=user.username,
            refresh_token=refresh_token,
            created_at=datetime.utcnow(),
            disabled=False,
        )
    )

    return TokenResponse(
        access_token=token, refresh_token=refresh_token, token_type=TokenTypeEnum.bearer
    )


@router.post("/refresh_token", response_model=TokenResponse)
def refresh_token(session: DependsSession, _input: RefreshTokenInput) -> TokenResponse:
    # https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/#Refresh-Token-Automatic-Reuse-Detection
    refresh_token_registry = RefreshTokenRegistry(session)
    try:
        refresh_token = decode_refresh_token(_input.refresh_token)
        username = refresh_token.sub
    except jwt.exceptions.ExpiredSignatureError:
        raise RefreshTokenExpiredException()
    except jwt.exceptions.PyJWTError as e:
        logger.error(f"Error when decoding refreshtoken {token}. Error is {e}")
        raise AuthenticationException()

    # This means that someone could have stolen a refresh token
    # Because the frontend should always use recent non-disabled refresh_tokens
    # We disabled all tokens according to the Automatic-reuse-detection protocol
    # https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/#Refresh-Token-Automatic-Reuse-Detection

    if refresh_token_registry.get(_input.refresh_token).disabled:
        refresh_token_registry.invalidate_all_refresh_tokens(username)
        raise RefreshTokenDisabledException()

    new_access_token = create_access_token(username)
    new_refresh_token = create_refresh_token(username)

    # Must save refresh_token in DB
    refresh_token_registry.invalidate_all_refresh_tokens(username)
    refresh_token_registry.create(
        RefreshTokenRegistryInput(
            username=username,
            refresh_token=new_refresh_token,
            created_at=datetime.utcnow(),
            disabled=False,
        )
    )

    return TokenResponse(
        access_token=new_access_token,
        refresh_token=new_refresh_token,
        token_type=TokenTypeEnum.bearer,
    )


@router.post("/google_login")
def google_login(session: DependsSession, _input: GoogleLoginInput):
    try:
        CLIENT_ID = settings.GOOGLE_CLIENT_ID
        idinfo = id_token.verify_oauth2_token(
            _input.google_token, requests.Request(), CLIENT_ID
        )
    except ValueError as e:
        logger.error(e)
        raise InvalidGoogleToken()

    google_info = GoogleIdInfo(**idinfo)

    if google_info.exp < datetime.utcnow().timestamp():
        logger.error(f"Expired google token {_input.google_token}")
        raise InvalidGoogleToken()
    if not google_info.email_verified:
        logger.error(f"Not verified google email {_input.google_token}")
        raise InvalidGoogleToken()

    # From there on, the token is valid & user is verified
    username = google_info.email
    user_registry = UserRegistry(session)
    user = user_registry.get(username)

    if not user:
        # First connexion, save user in DB
        user = user_registry.create(
            UserRegistryInput(
                username=username,
                password=None,
                disabled=False,
                created_at=datetime.utcnow(),
                source=UserSource.google,
            )
        )

    new_access_token = create_access_token(username)
    new_refresh_token = create_refresh_token(username)

    # Must save refresh_token in DB
    refresh_token_registry = RefreshTokenRegistry(session)
    refresh_token_registry.invalidate_all_refresh_tokens(username)
    refresh_token_registry.create(
        RefreshTokenRegistryInput(
            username=username,
            refresh_token=new_refresh_token,
            created_at=datetime.utcnow(),
            disabled=False,
        )
    )

    return TokenResponse(
        access_token=new_access_token,
        refresh_token=new_refresh_token,
        token_type=TokenTypeEnum.bearer,
    )
