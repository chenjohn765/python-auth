import logging
from datetime import datetime, timedelta

import jwt
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session

from src.exceptions.http_exceptions import AuthenticationException
from src.api.auth.pydantics import RefreshToken, Token
from src.models import User as DBUser
from src.registries.user import UserRegistry
from src.utils.hash import verify_pwd

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

# https://www.youtube.com/watch?v=5GxQ1rLTwaU guide for setting up auth
#  UUID
ACCESS_TOKEN_SECRET_KEY = "b4589bb4-da7c-4d2c-99f8-4455164cd1d2"
REFRESH_TOKEN_SECRET_KEY = "226a4b9d-6d90-4b55-befe-88fdd7dfc0a4"
PWD_HASHING_ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 20
REFRESH_TOKEN_EXPIRE_MINUTES = 20 * 10
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def create_access_token(username: str) -> str:
    start_time = datetime.utcnow()
    exp = start_time + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    payload = Token(sub=username, exp=exp).dict()
    # This line is just used for validating the pydantic model
    token = jwt.encode(payload, ACCESS_TOKEN_SECRET_KEY, PWD_HASHING_ALGORITHM)
    return token


def create_refresh_token(username: str) -> str:
    start_time = datetime.utcnow()
    exp = start_time + timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES)

    payload = RefreshToken(sub=username, exp=exp).dict()
    # This line is just used for validating the pydantic model
    token = jwt.encode(payload, REFRESH_TOKEN_SECRET_KEY, PWD_HASHING_ALGORITHM)
    return token


def decode_access_token(token: str) -> Token:
    payload = jwt.decode(
        token, ACCESS_TOKEN_SECRET_KEY, algorithms=[PWD_HASHING_ALGORITHM]
    )
    try:
        token = Token(
            sub=payload.get("sub"),
            exp=datetime.fromtimestamp(payload.get("exp")),
        )
    except Exception:
        logger.error(
            f"Decoding acces token {token} but invalid format. payload is: {payload}"
        )
        raise AuthenticationException()

    return token


def decode_refresh_token(token: str) -> RefreshToken:
    payload = jwt.decode(
        token, REFRESH_TOKEN_SECRET_KEY, algorithms=[PWD_HASHING_ALGORITHM]
    )
    try:
        token = RefreshToken(
            sub=payload.get("sub"),
            exp=datetime.fromtimestamp(payload.get("exp")),
        )
    except Exception:
        logger.error(
            f"Decoding refresh token {token} but invalid format. payload is: {payload}"
        )
        raise AuthenticationException()

    return token


def fetch_user(session: Session, username: str) -> DBUser | None:
    user_registry = UserRegistry(session)
    user = user_registry.get(username)
    return user


def authenticate_user(session: Session, username: str, pwd: str) -> DBUser | None:
    user = fetch_user(session, username)
    if not user:
        return None
    if not verify_pwd(pwd, user.hashed_password):
        return None
    return user
