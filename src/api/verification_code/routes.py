from fastapi import APIRouter

from src.api.auth.pydantics import (
    VerificationCodeSendInput,
    VerificationCodeSendResponse,
)
from src.registries.verification_code import (
    VerificationCodeRegistry,
)
from src.api.utils.depends.depends import DependsSession
from src.email.email import send_email
from src.email.aws_email import send_aws_email
import logging


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
router = APIRouter()


@router.post("/verification_code/", response_model=VerificationCodeSendResponse)
def send_verification_code(
    session: DependsSession, _input: VerificationCodeSendInput
) -> VerificationCodeSendResponse:
    verification_code_registry = VerificationCodeRegistry(session)
    db_object = verification_code_registry.generate(_input.destination)

    # Send email with the code to destination
    send_email(
        _input.destination, "Verification Code", f"Verification code: {db_object.value}"
    )
    # send_aws_email(
    #     _input.destination, "Verification code", f"Verification code: {db_object.value}"
    # )

    return VerificationCodeSendResponse(id=db_object.id)


# # Only used for testing
# @router.post("/verification_code/validate")
# def validate_verification_code(
#     session: DependsSession, _input: VerificationCodeSendResponse
# ) -> dict:
#     verification_code_registry = VerificationCodeRegistry(session)
#     db_object = verification_code_registry.validate_code(_input.id)

#     # Send email with the code to destination

#     return {"status": 200}
