from datetime import datetime
from enum import Enum as PythonEnum
from typing import Any

from sqlalchemy import (
    Boolean,
    DateTime,
    Enum,
    ForeignKey,
    String,
    CheckConstraint,
    Integer,
    UUID as SQLUUID,
)
from sqlalchemy.orm import Mapped, declarative_base, mapped_column, relationship
from uuid import UUID, uuid4

# convention = {
#     "ix": "ix_%(column_0_label)s",
#     "uq": "uq_%(table_name)s_%(column_0_name)s",
#     "ck": "ck_%(table_name)s_%(constraint_name)s",
#     "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
#     "pk": "pk_%(table_name)s",
# }


Base: Any = declarative_base()
metadata = Base.metadata


class UserSource(PythonEnum):
    backend = "backend"
    google = "google"
    twitter = "twitter"
    instagram = "instagram"
    facebook = "facebook"


class User(Base):
    __tablename__ = "user"
    # Username should be an email
    username: Mapped[str] = mapped_column(primary_key=True, nullable=False)
    source: Mapped[UserSource] = mapped_column(Enum(UserSource), nullable=False)
    hashed_password: Mapped[str] = mapped_column(String(256), nullable=True)
    disabled: Mapped[bool] = mapped_column(Boolean, nullable=False)
    created_at: Mapped[datetime] = mapped_column(DateTime, nullable=False)
    __table_args__ = (
        CheckConstraint(
            "(source = 'backend') = (hashed_password IS NOT NULL)",
            name="password_only_for_backend",
        ),
    )


class RefreshToken(Base):
    __tablename__ = "refresh_token"
    hashed_refresh_token: Mapped[str] = mapped_column(
        String(256), primary_key=True, nullable=False
    )
    created_at: Mapped[datetime] = mapped_column(DateTime, nullable=False)
    disabled: Mapped[bool] = mapped_column(Boolean, nullable=False)
    # Relationship
    # Username should be an email
    username: Mapped[str] = mapped_column(ForeignKey("user.username"))
    user: Mapped[User] = relationship()


class VerificationCode(Base):
    __tablename__ = "verification_code"
    id: Mapped[UUID] = mapped_column(SQLUUID, primary_key=True, default=uuid4)
    value: Mapped[int] = mapped_column(Integer, nullable=False)
    start_date: Mapped[datetime] = mapped_column(DateTime, nullable=False)
    end_date: Mapped[datetime] = mapped_column(DateTime, nullable=False)
    used: Mapped[bool] = mapped_column(Boolean, nullable=False, default=False)
    destination: Mapped[str] = mapped_column(String, nullable=False)
