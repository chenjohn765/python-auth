from datetime import datetime
from typing import Any, Dict, List

from pydantic import BaseModel
from sqlalchemy import select

from src.models import RefreshToken
from src.utils.hash import hash_refresh_token
from src.registries.base_registry import BaseRegistry


class WithHashedRefreshedToken:
    refresh_token: str

    @property
    def hashed_refresh_token(self) -> str:
        return hash_refresh_token(self.refresh_token)


class RefreshTokenRegistryInput(BaseModel, WithHashedRefreshedToken):
    username: str
    refresh_token: str
    created_at: datetime
    disabled: bool


class RefreshTokenRegistryUpdate(BaseModel):
    disabled: bool


class RefreshTokenRegistry(BaseRegistry):
    db_model = RefreshToken

    def create(self, _input: RefreshTokenRegistryInput) -> RefreshToken:
        db_object = RefreshToken(
            username=_input.username,
            hashed_refresh_token=_input.hashed_refresh_token,
            created_at=_input.created_at,
            disabled=_input.disabled,
        )
        self.session.add(db_object)
        self.session.commit()
        return db_object

    def list(self, filters: Dict[str, List | Any] = {}) -> List[RefreshToken]:
        query = select(RefreshToken)
        if filters:
            for key, value in filters.items():
                column = getattr(RefreshToken, key)
                if isinstance(value, List):
                    query.where(column.in_(value))
                else:
                    query.where(column == value)

        db_objects = self.session.scalars(query)
        return db_objects

    def get(self, raw_refresh_token: str) -> RefreshToken:
        hashed_refresh_token = hash_refresh_token(raw_refresh_token)
        return self.session.get(RefreshToken, hashed_refresh_token)

    def update(
        self, raw_refresh_token: str, _update: RefreshTokenRegistryUpdate
    ) -> RefreshToken:
        db_object = self.get(raw_refresh_token)
        db_object.disabled = _update.disabled
        self.session.commit(db_object)
        return db_object

    def invalidate_all_refresh_tokens(self, username: str) -> None:
        db_objects_of_username = self.list(
            filters={"username": username, "disabled": False}
        )
        for x in db_objects_of_username:
            x.disabled = True

        self.session.add_all(db_objects_of_username)
        self.session.commit()
