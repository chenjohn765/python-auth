from sqlalchemy.orm import Session
from abc import ABC, abstractmethod
from src.exceptions.http_exceptions import EntityNotFoundException
from src.models import Base


class BaseRegistry(ABC):
    @property
    @abstractmethod
    def db_model(self) -> Base:
        """
        Property to define to indicate which db model the registry handles
        """
        pass

    def __init__(self, session: Session) -> None:
        self.session = session

    @abstractmethod
    def create(self, _input: ...) -> ...:
        pass

    @abstractmethod
    def list(self) -> ...:
        pass

    @abstractmethod
    def get(self, pk: ...) -> ...:
        pass

    def getOrFail(self, pk: ...) -> ...:
        db_object = self.get(pk)
        if db_object is None:
            # Will return the name of the class.
            entity = self.db_model.__table__.name
            raise EntityNotFoundException(entity, pk)

        return db_object

    @abstractmethod
    def update(self, pk: ..., _update: ...) -> ...:
        pass
