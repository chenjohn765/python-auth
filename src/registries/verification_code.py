from datetime import datetime, timedelta
from typing import Any, Dict, List

from pydantic import BaseModel
from sqlalchemy import select

from src.models import VerificationCode
from src.registries.base_registry import BaseRegistry
from src.exceptions.http_exceptions import (
    VerificationCodeExpiredException,
    VerificationCodeUsedException,
    WrongVerificationCodeException,
)
from random import randint


class VerificationCodeRegistryInput(BaseModel):
    value: str
    start_date: datetime
    end_date: datetime
    used: bool
    destination: str


class VerificationCodeRegistryUpdate(BaseModel):
    used: bool


class VerificationCodeRegistry(BaseRegistry):
    db_model = VerificationCode

    def create(self, _input: VerificationCodeRegistryInput) -> VerificationCode:
        db_object = VerificationCode(
            value=_input.value,
            start_date=_input.start_date,
            end_date=_input.end_date,
            used=_input.used,
            destination=_input.destination,
        )
        self.session.add(db_object)
        self.session.commit()
        return db_object

    def generate(self, destination: str) -> VerificationCode:
        start_date = datetime.now()
        return self.create(
            VerificationCodeRegistryInput(
                value=randint(0, 999999),
                start_date=datetime.now(),
                end_date=start_date + timedelta(minutes=1),
                used=False,
                destination=destination,
            )
        )

    def list(self, filters: Dict[str, List | Any] = {}) -> List[VerificationCode]:
        query = select(VerificationCode)
        if filters:
            for key, value in filters.items():
                column = getattr(VerificationCode, key)
                if isinstance(value, List):
                    query.where(column.in_(value))
                else:
                    query.where(column == value)

        db_objects = self.session.scalars(query)
        return db_objects

    def get(self, id: str) -> VerificationCode:
        return self.session.get(VerificationCode, id)

    def update(
        self, id: str, _update: VerificationCodeRegistryUpdate
    ) -> VerificationCode:
        db_object: VerificationCode = self.getOrFail(id)
        db_object.used = _update.used

        self.session.add(db_object)
        self.session.commit()
        return db_object

    def validate_code(self, id: str, value: int):
        db_object: VerificationCode = self.getOrFail(id)

        if db_object.value != value:
            raise WrongVerificationCodeException()
        if datetime.now() > db_object.end_date:
            raise VerificationCodeExpiredException()
        if db_object.used:
            raise VerificationCodeUsedException()

        self.update(id, VerificationCodeRegistryUpdate(used=True))
        return db_object
