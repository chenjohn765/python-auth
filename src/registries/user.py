from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel
from sqlalchemy import select
from src.registries.base_registry import BaseRegistry

from src.models import User, UserSource
from src.utils.hash import hash_pwd


class WithHashedPassword:
    password: Optional[str]

    @property
    def hashed_password(self) -> Optional[str]:
        if not self.password:
            return None
        return hash_pwd(self.password)


class UserRegistryInput(BaseModel, WithHashedPassword):
    username: str
    password: Optional[str]
    disabled: bool
    created_at: datetime
    source: UserSource


class UserRegistryUpdate(BaseModel, WithHashedPassword):
    username: str
    password: str
    disabled: bool


class UserRegistry(BaseRegistry):
    db_model = User

    def create(self, _input: UserRegistryInput) -> User:
        db_object = User(
            username=_input.username,
            hashed_password=_input.hashed_password,
            created_at=_input.created_at,
            disabled=_input.disabled,
            source=_input.source,
        )
        self.session.add(db_object)
        self.session.commit()
        return db_object

    def list(self) -> List[User]:
        query = select(User)
        db_objects = self.session.scalars(query)
        return db_objects

    def get(self, username: str) -> User:
        return self.session.get(User, username)

    def update(self, username: str, _update: UserRegistryUpdate) -> User:
        db_object = self.get(username)
        db_object.username = _update.username
        db_object.hashed_password = _update.hashed_password
        db_object.disabled = _update.disabled
        self.session.commit(db_object)
        return db_object
