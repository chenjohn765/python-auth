from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from alembic import command
from alembic.config import Config
from src.settings import settings

db_url = f"postgresql+psycopg2://{settings.DB_USERNAME}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
engine = create_engine(db_url)


def get_session():
    with Session(engine) as session:
        yield session


def run_migrations(lv: int = 1) -> None:
    # https://stackoverflow.com/questions/24622170/using-alembic-api-from-inside-application-code
    config_file = "alembic.ini"
    alembic_cfg = Config(file_=config_file)

    if lv:
        command.upgrade(alembic_cfg, "head")
    else:
        command.downgrade(alembic_cfg, "base")
