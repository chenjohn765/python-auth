import hashlib

from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_pwd(plain_pwd: str, hashed_pwd: str) -> bool:
    return pwd_context.verify(plain_pwd, hashed_pwd)


def hash_pwd(pwd: str) -> str:
    return pwd_context.hash(pwd)


def hash_refresh_token(token: str) -> str:
    h = hashlib.new("sha256")
    h.update(token.encode("utf-8"))
    return h.hexdigest()
