from fastapi.exceptions import HTTPException
from typing import Any, Dict, Optional


class CustomBaseException(HTTPException):
    def __init__(
        self,
        status_code: int,
        error_code: int | str,
        detail: Any,
        headers: Optional[Dict[str, str]] = None,
    ) -> None:
        super().__init__(
            status_code=status_code,
            detail={
                "message": detail,
                "error_code": error_code,
                "error_class": type(self).__name__,
            },
            headers=headers,
        )
