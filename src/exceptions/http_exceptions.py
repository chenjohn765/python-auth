from src.exceptions.base_exceptions import CustomBaseException
from typing import Dict, Tuple


# ================== 400 ==================================
class DisabledUserException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=400, error_code="400DU", detail="User was disabled."
        )


# ==================== 401 ====================================
class AuthenticationException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401A",
            detail="Authentication Failed",
            headers={"WWW-Authenticate": "Bearer"},
        )


class WrongCredentialsException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401WC",
            detail="Wrong username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )


class TokenExpiredException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401TE",
            detail="Token expired.",
            # detail={"message": "Token expired.", "code": 40101},
        )


class RefreshTokenExpiredException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401RTE",
            detail="Refresh token expired.",
        )


class RefreshTokenDisabledException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401RTD",
            detail="Refresh token is disabled. Please log in again.",
        )


class InvalidGoogleToken(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=401,
            error_code="401IGT",
            detail="Google token is invalid.",
        )


# ==================== 404 ====================================


class EntityNotFoundException(CustomBaseException):
    def __init__(self, entity: str, pk: str | Tuple | Dict) -> None:
        super().__init__(
            status_code=404,
            error_code="404ENF",
            detail=f"{entity} {pk} not found.",
        )


class UserDoesNotExistException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=404,
            error_code="404UDNE",
            detail="User does not exist",
        )


# ==================== 422 ====================================


class UsernameAlreadyExistsException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            error_code="422UAE",
            detail="Username already exists.",
            headers={"WWW-Authenticate": "Bearer"},
        )


class VerificationCodeExpiredException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            error_code="422VCE",
            detail="Verification code expired",
        )


class VerificationCodeUsedException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            error_code="422VCU",
            detail="Verification code already used",
        )


class WrongVerificationCodeException(CustomBaseException):
    def __init__(self) -> None:
        super().__init__(
            status_code=422,
            error_code="422WVC",
            detail="Wrong verification code",
        )
