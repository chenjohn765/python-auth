# Setup Alembic & SQLAlchemy

- Follow this guide https://medium.com/@johnidouglasmarangon/using-migrations-in-python-sqlalchemy-with-alembic-docker-solution-bd79b219d6a

# AWS resource already pre-existing

- The jchen-python-auth bucket was created by hand
- The ECR johnny-ecr was created by hand
  They are both entities that we assume pre-existing (and not handled by terraform)
- A AWSServiceRoleForECS globally created for ECS
- A VPC endpoint pointing to ECR so that ECS can pull images from ECR, without passing by internet.
  This is required for FARGATE launch type.
- The domain jcauth.com was registered by hand on AWS.

# Create your docker image & push to ECR:

- run `make dev_build`
- run `make dev_push`

This will build the docker image and push it to ECR

# Deploy to AWS

To deploy to AWS we need:

- a RDS instance for the database
- a ECS cluster to deploy the docker image
- a LogGroup to put log in

# Terraform usage

## 1. Install Terraform

Just find some documentation to install it.

## 2. Init terraform

- Go to /terraform/live/dev folder.
- Make sure you have configured your AWS profile. `aws configure ...`
- Terraform will use the default profile by default.

- If you are using a s3 remote backend to store the state (just like here), then make sure:
  (
  Based from this documentation https://spacelift.io/blog/terraform-s3-backend
  and https://developer.hashicorp.com/terraform/language/settings/backends/s3
  )
- a. create a S3 bucket with the name defined in the terraform configuration
- b. create a dynamoDB table with the name defined in the terraform configuration, with "LockID" as column name
- c. create a user destined to terraform usage with the requried policies:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::mybucket"
    },
    {
      "Effect": "Allow",
      "Action": ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"],
      "Resource": "arn:aws:s3:::mybucket/path/to/my/key"
    },
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:DescribeTable",
        "dynamodb:GetItem",
        "dynamodb:PutItem",
        "dynamodb:DeleteItem"
      ],
      "Resource": "arn:aws:dynamodb:*:*:table/mytable"
    }
    // + other policies you will need to create resources
  ]
}
```

- run `terraform init`
- run `terraform plan`
- run `terraform apply`
