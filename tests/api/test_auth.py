import time
from unittest.mock import patch, ANY

import pytest
from fastapi.testclient import TestClient

from src.exceptions.http_exceptions import (
    AuthenticationException,
    RefreshTokenDisabledException,
    TokenExpiredException,
)
from sqlalchemy.orm import Session
from src.registries.user import UserRegistry
from datetime import datetime, timedelta


@pytest.mark.usefixtures("registered_user")
class TestToken:
    def test_token_route_should_return_access_token_and_refresh_token_if_registered(
        self, test_client: TestClient
    ) -> None:
        # When
        response = test_client.post(
            "/token", data={"username": "toto", "password": "toto"}
        )
        # then
        assert response.status_code == 200
        assert response.json()["access_token"]
        assert response.json()["refresh_token"]

    def test_token_route_token_should_be_able_to_authentify(
        self,
        test_client: TestClient,
    ) -> None:
        # Given
        response = test_client.post(
            "/token", data={"username": "toto", "password": "toto"}
        )
        token = response.json()["access_token"]
        # When
        response = test_client.get(
            "/users/me", headers={"Authorization": f"Bearer {token}"}
        )
        # Then
        assert response.status_code == 200

    def test_token_should_not_work_if_expired(self, test_client: TestClient) -> None:
        # Given
        with patch("src.api.auth.utils.ACCESS_TOKEN_EXPIRE_MINUTES", 0):
            response = test_client.post(
                "/token", data={"username": "toto", "password": "toto"}
            )
            token = response.json()["access_token"]
        # When
        response = test_client.get(
            "/users/me", headers={"Authorization": f"Bearer {token}"}
        )
        # Then
        assert response.status_code == TokenExpiredException().status_code
        assert response.json() == {"detail": TokenExpiredException().detail}

    def test_wrong_token_should_not_work(self, test_client: TestClient) -> None:
        # Given
        with patch("src.api.auth.utils.ACCESS_TOKEN_SECRET_KEY", "wrong_secret_key"):
            response = test_client.post(
                "/token", data={"username": "toto", "password": "toto"}
            )
            token = response.json()["access_token"]
        # When
        response = test_client.get(
            "/users/me", headers={"Authorization": f"Bearer {token}"}
        )
        # Then
        assert response.status_code == AuthenticationException().status_code
        assert response.json() == {"detail": AuthenticationException().detail}


@pytest.mark.usefixtures("registered_user")
class TestRefreshToken:
    def test_refresh_token_route_should_return_access_token_and_refresh_token(
        self, test_client: TestClient
    ) -> None:
        # Given
        response = test_client.post(
            "/token", data={"username": "toto", "password": "toto"}
        )
        access_token = response.json()["access_token"]
        refresh_token = response.json()["refresh_token"]

        # When
        # Sleep 1s is important, otherwise it will return the same tokens
        time.sleep(1)
        response = test_client.post(
            "/refresh_token", json={"refresh_token": refresh_token}
        )
        # Then
        assert response.status_code == 200
        new_access_token = response.json()["access_token"]
        new_refresh_token = response.json()["refresh_token"]

        assert new_access_token != access_token
        assert new_refresh_token != refresh_token

        # Try to use this new access_token
        response = test_client.get(
            "/users/me", headers={"Authorization": f"Bearer {new_access_token}"}
        )
        assert response.status_code == 200

    def test_refresh_token_route_should_disable_all_refresh_tokens_if_possible_fraud(
        self, test_client: TestClient
    ) -> None:
        # Given
        response = test_client.post(
            "/token", data={"username": "toto", "password": "toto"}
        )
        first_refresh_token = response.json()["refresh_token"]

        # We then use this refresh token
        # Sleep 1s is important, otherwise it will return the same tokens
        time.sleep(1)
        response = test_client.post(
            "/refresh_token", json={"refresh_token": first_refresh_token}
        )
        second_refresh_token = response.json()["refresh_token"]

        # When
        # Some fraud is using our old token. (Or maybe that we are using ours after the fraud)
        response = test_client.post(
            "/refresh_token", json={"refresh_token": first_refresh_token}
        )
        # Then
        assert response.status_code == RefreshTokenDisabledException().status_code
        assert response.json() == {"detail": RefreshTokenDisabledException().detail}

        # And the newest refresh_token should be diabled too
        response = test_client.post(
            "/refresh_token", json={"refresh_token": second_refresh_token}
        )
        assert response.status_code == RefreshTokenDisabledException().status_code
        assert response.json() == {"detail": RefreshTokenDisabledException().detail}

    def test_wrong_refresh_token_should_not_work(self, test_client: TestClient) -> None:
        # When
        response = test_client.post(
            "/refresh_token", json={"refresh_token": "wrong_refresh_token"}
        )
        # Then
        assert response.status_code == AuthenticationException().status_code
        assert response.json() == {"detail": AuthenticationException().detail}


class TestGoogleLogin:
    def test_google_login_should_create_user_in_db_if_first_login(
        self, test_client: TestClient, session: Session
    ) -> None:
        # When
        with patch(
            "src.api.auth.routes.id_token.verify_oauth2_token",
            return_value={
                "iss": "https://accounts.google.com/",
                # identifier like 102091680547620533731
                "sub": "102091680547620533731",
                "iat": datetime.utcnow().timestamp(),
                "exp": (datetime.utcnow() + timedelta(minutes=10)).timestamp(),
                "email": "mock_email.gmail.com",
                "email_verified": True,
                "name": "mock_email",
            },
        ) as mock_verify_oauth2_token:
            test_client.post(
                "/google_login", json={"google_token": "mock_google_token"}
            )

        mock_verify_oauth2_token.assert_called_once_with("mock_google_token", ANY, ANY)
        user_registry = UserRegistry(session)
        user = user_registry.get("mock_email.gmail.com")
        assert user.username == "mock_email.gmail.com"

    def test_google_login_should_return_access_token(
        self,
        test_client: TestClient,
    ) -> None:
        # When
        with patch(
            "src.api.auth.routes.id_token.verify_oauth2_token",
            return_value={
                "iss": "https://accounts.google.com/",
                # identifier like 102091680547620533731
                "sub": "102091680547620533731",
                "iat": datetime.utcnow().timestamp(),
                "exp": (datetime.utcnow() + timedelta(minutes=10)).timestamp(),
                "email": "mock_email.gmail.com",
                "email_verified": True,
                "name": "mock_email",
            },
        ):
            response = test_client.post(
                "/google_login", json={"google_token": "mock_google_token"}
            )
        assert response.status_code == 200
        assert response.json()["access_token"]
        assert response.json()["refresh_token"]
