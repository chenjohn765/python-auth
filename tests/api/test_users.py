from fastapi.testclient import TestClient

from src.exceptions.http_exceptions import (
    UsernameAlreadyExistsException,
    WrongVerificationCodeException,
)
from sqlalchemy.orm import Session
from src.registries.user import UserRegistry
from src.registries.verification_code import VerificationCodeRegistry


class TestRegister:
    def test_register_should_register_user(
        self, test_client: TestClient, session: Session
    ) -> None:
        # When
        verification_code_registry = VerificationCodeRegistry(session)
        verification_code = verification_code_registry.generate(destination="toto")

        response = test_client.post(
            "/users/register",
            data={
                "username": "toto",
                "password": "toto",
                "verification_id": verification_code.id,
                "verification_code": verification_code.value,
            },
        )

        # then
        assert response.status_code == 200
        assert response.json()["username"] == "toto"

        user_registry = UserRegistry(session)
        db_object = user_registry.get("toto")
        assert db_object.username == "toto"

    def test_register_should_raise_if_user_already_exists(
        self, test_client: TestClient, session: Session
    ) -> None:
        # Given
        verification_code_registry = VerificationCodeRegistry(session)
        verification_code = verification_code_registry.generate(destination="toto")
        response = test_client.post(
            "/users/register",
            data={
                "username": "toto",
                "password": "toto",
                "verification_id": verification_code.id,
                "verification_code": verification_code.value,
            },
        )
        assert response.status_code == 200
        # When

        # We must fetch a new verification code
        verification_code_registry = VerificationCodeRegistry(session)
        verification_code = verification_code_registry.generate(destination="toto")
        response = test_client.post(
            "/users/register",
            data={
                "username": "toto",
                "password": "toto",
                "verification_id": verification_code.id,
                "verification_code": verification_code.value,
            },
        )
        assert response.status_code == UsernameAlreadyExistsException().status_code
        assert response.json() == {"detail": UsernameAlreadyExistsException().detail}

    def test_register_should_fail_if_verification_code_fails(
        self, test_client: TestClient, session: Session
    ) -> None:
        # Given
        verification_code_registry = VerificationCodeRegistry(session)
        verification_code = verification_code_registry.generate(destination="toto")

        # When
        response = test_client.post(
            "/users/register",
            data={
                "username": "toto",
                "password": "toto",
                "verification_id": verification_code.id,
                "verification_code": 1111111,
            },
        )
        assert response.status_code == WrongVerificationCodeException().status_code
        assert response.json() == {"detail": WrongVerificationCodeException().detail}
