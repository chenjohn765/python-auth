from datetime import datetime

import pytest
from sqlalchemy.orm import Session

from src.connect import get_session, run_migrations
from src.models import User
from src.registries.user import UserRegistry, UserRegistryInput, UserSource


# Scope must be function & we must close the session
# because we kill & reset DB at every test function.
# This will avoid connection to still exist while we destroy
@pytest.fixture()
def session():
    session = next(get_session())
    yield session

    session.close()


@pytest.fixture()
def registered_user(session: Session) -> User:
    user_registry = UserRegistry(session)
    db_object = user_registry.create(
        UserRegistryInput(
            username="toto",
            password="toto",
            disabled=False,
            created_at=datetime.utcnow(),
            source=UserSource.backend,
        )
    )
    return db_object


@pytest.fixture(scope="function", autouse=True)
def resetdb():
    run_migrations(lv=0)
    run_migrations(lv=1)
