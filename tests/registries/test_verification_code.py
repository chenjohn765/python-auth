import pytest

from sqlalchemy.orm import Session
from src.registries.verification_code import (
    VerificationCodeRegistry,
)
from src.exceptions.http_exceptions import (
    VerificationCodeUsedException,
    VerificationCodeExpiredException,
    WrongVerificationCodeException,
)
from datetime import datetime, timedelta


class TestVerificationCodeRegistry:
    def test_validate_code_should_update_code_as_used(self, session: Session) -> None:
        verification_code_registry = VerificationCodeRegistry(session)
        db_object = verification_code_registry.generate("destination@gmail.com")

        verification_code_registry.validate_code(db_object.id, db_object.value)

    def test_validate_code_should_raise_if_wrong_value(self, session: Session) -> None:
        verification_code_registry = VerificationCodeRegistry(session)
        db_object = verification_code_registry.generate("destination@gmail.com")

        with pytest.raises(WrongVerificationCodeException):
            verification_code_registry.validate_code(db_object.id, 1111111)

    def test_validate_code_should_raise_if_already_validated(
        self, session: Session
    ) -> None:
        verification_code_registry = VerificationCodeRegistry(session)
        db_object = verification_code_registry.generate("destination@gmail.com")

        verification_code_registry.validate_code(db_object.id, db_object.value)

        with pytest.raises(VerificationCodeUsedException):
            verification_code_registry.validate_code(db_object.id, db_object.value)

    def test_validate_code_should_raise_if_expired(self, session: Session) -> None:
        verification_code_registry = VerificationCodeRegistry(session)
        db_object = verification_code_registry.generate("destination@gmail.com")

        # Fake expiration date
        db_object.end_date = datetime.now() - timedelta(minutes=1)
        session.add(db_object)
        session.commit()

        with pytest.raises(VerificationCodeExpiredException):
            verification_code_registry.validate_code(db_object.id, db_object.value)
