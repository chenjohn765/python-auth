import pytest

from sqlalchemy.orm import Session
from src.registries.user import UserRegistry, UserRegistryInput, UserSource
from datetime import datetime
from sqlalchemy.exc import IntegrityError


class TestUserRegistry:
    def test_create_should_fail_if_password_incoherent_with_user_source(
        self, session: Session
    ) -> None:
        user_registry = UserRegistry(session)
        # No password but is from backend
        with pytest.raises(IntegrityError, match="password_only_for_backend"):
            user_registry.create(
                UserRegistryInput(
                    username="toto",
                    password=None,
                    disabled=False,
                    created_at=datetime.utcnow(),
                    source=UserSource.backend,
                )
            )
        session.rollback()

        # Has password but is from other source than backend
        with pytest.raises(IntegrityError, match="password_only_for_backend"):
            user_registry.create(
                UserRegistryInput(
                    username="toto",
                    password="toto",
                    disabled=False,
                    created_at=datetime.utcnow(),
                    source=UserSource.google,
                )
            )
        session.rollback()
