import subprocess
import argparse


def get_last_commit_sha():
    try:
        # Run 'git rev-parse HEAD' command and capture the output
        last_commit_sha = subprocess.check_output(
            ["git", "rev-parse", "HEAD"], universal_newlines=True
        ).strip()
        return last_commit_sha
    except subprocess.CalledProcessError as e:
        print(f"Error: Unable to get the last commit SHA. {e}")
        return None


def build_docker(env: str):
    last_commit = get_last_commit_sha()[0:5]
    base_docker_tag = f"{env}-python-auth"
    detailed_docker_tag = f"{base_docker_tag}-{last_commit}"
    subprocess.run(["docker", "build", "-t", base_docker_tag, "."])
    # clone the image to another tag to know which was the last commit
    being_cloned = base_docker_tag
    clone_product = detailed_docker_tag
    subprocess.run(
        [
            "docker",
            "tag",
            f"{being_cloned}:latest",
            clone_product,
        ]
    )


if __name__ == "__main__":
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # Add the arguments you want to parse
    parser.add_argument("env", type=str, help="dev|uat|prod")

    # Parse the command-line arguments
    args = parser.parse_args()

    build_docker(args.env)
