import subprocess
import argparse
import os

ECR_URL = "531861524173.dkr.ecr.eu-central-1.amazonaws.com/johnny-ecr"
ECR_PATH = f"{ECR_URL}"


def push_docker(env: str):
    docker_tag = f"{env}-python-auth"

    # clone the image to another tag to know which was the last commit
    being_cloned = docker_tag
    ecr_tag = docker_tag
    clone_product = f"{ECR_URL}:{ecr_tag}"
    subprocess.run(
        [
            "docker",
            "tag",
            f"{being_cloned}:latest",
            clone_product,
        ]
    )

    # Login to ECR
    subprocess.run(
        [
            "aws",
            "ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin ${ECR_URL}",
        ]
    )

    subprocess.run(
        [
            "docker",
            "push",
            clone_product,
        ]
    )


if __name__ == "__main__":
    # Create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # Add the arguments you want to parse
    parser.add_argument("env", type=str, help="dev|uat|prod")

    # Parse the command-line arguments
    args = parser.parse_args()

    push_docker(args.env)
