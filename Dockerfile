# Use the official Python image as the base image
FROM python:3.10-slim
# Set the working directory inside the container
WORKDIR /app
# Copy the requirements file into the container
COPY requirements.txt .
# Install the required packages using pip
RUN pip install --no-cache-dir -r requirements.txt

# Copy the entire current directory into the container
COPY src src
COPY alembic alembic 
COPY alembic.ini alembic.ini 


# Expose the port on which the FastAPI app will run (change this to your desired port)
EXPOSE 8000

# Define the command to run your FastAPI app when the container starts
# Run multiple commands using shell
CMD ["bash", "-c", "alembic upgrade head && uvicorn src.main:app --host 0.0.0.0 --port 8000"]
