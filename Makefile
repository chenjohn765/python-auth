SHELL := /bin/bash

.PHONY: server
server:
	set -a && source .env && set +a && uvicorn src.main:app --reload

# ================ MIGRRATIONS ============================
.PHONY: createm
createm:
	set -a && source .env && set +a && alembic revision --autogenerate -m $(n)

.PHONY: migrate
migrate:
	set -a && source .env && set +a && alembic upgrade head

.PHONY: revert
revert:
	set -a && source .env && set +a && alembic downgrade -1

# ================ DB ============================
.PHONY: db
db:
	docker-compose up -d db

.PHONY: testdb
testdb:
	docker-compose -f docker-compose.test.yml up -d testdb

.PHONY: resetdb
resetdb:
	alembic downgrade base
	alembic upgrade head

.PHONY: formatdb
formatdb:
	alembic downgrade base

# ================ TESTS ============================
.PHONY: test
test:
	docker-compose -f docker-compose.test.yml down -v || true
	make testdb
# Need to sleep, otherwise, it will fail.
	sleep 0.5
	set -a && source .env.test && set +a && pytest tests/ $(c)

# ================ QUALITY ============================
.PHONY: quality
quality:
	flake8 .
	isort . --check --diff
	mypy .
	diff -q requirements.txt <(poetry export)

.PHONY: format
format:
	black .
	isort .

.PHONY: requirements
requirements:
	poetry export -o requirements.txt

# ================ DOCKER IMAGE ========================
.PHONY: dev_build
dev_build:
	python3 commands/build_docker.py dev
	docker container prune

.PHONY: dev_push
dev_push:
	python3 commands/push_docker.py dev

.PHONY: dev_docker_app
dev_docker_app:
	docker-compose -f docker-compose.app.yml up
