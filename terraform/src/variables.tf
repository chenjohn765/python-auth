variable "ssm_path_db_password" {
  type        = string
  description = "SSM Path"
  default     = ""
}

variable "env" {
  type        = string
  description = "dev|uat|prod"
  default     = ""
}
