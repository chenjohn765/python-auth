resource "aws_cloudwatch_log_group" "python_auth" {
  name = "${local.prefix}_log_group"

  tags = {
    Environment = "dev"
    Application = "python_auth"
  }
}
resource "aws_ecs_cluster" "python_auth_cluster" {
  name = "${local.prefix}_ecs_cluster"

  configuration {
    execute_command_configuration {
        logging    = "OVERRIDE"
        log_configuration {
            cloud_watch_log_group_name     = aws_cloudwatch_log_group.python_auth.name
            s3_bucket_name = "jchen-python-auth"
            s3_key_prefix = "logs/ecs/python_auth"
      }
    }
  }
}

# Was created globally by hand
# # Required for the ECS cluster
# resource "aws_iam_service_linked_role" "ecs" {
#   aws_service_name = "ecs.amazonaws.com"
# }

# role to be assumed by the task for its execution
resource "aws_iam_role" "python_auth_task_role" {
  name = "${local.prefix}_task_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = [
            "ecs.amazonaws.com",
            "ecs-tasks.amazonaws.com"
          ]
        }
      },
    ]
  })

  # To allow the API to use the SES service
  inline_policy {
    name = "python_auth_send_email"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect   = "Allow"
          "Action": [
            "ses:*",
          ]
          "Resource": "*"
        },
      ]
    })
  }

  # Required for the creation of the ECS service
  # https://docs.aws.amazon.com/IAM/latest/UserGuide/using-service-linked-roles.html
  inline_policy {
    name = "python_auth_service_linked_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect   = "Allow"
          "Action": [
            "iam:CreateServiceLinkedRole",
            "iam:DeleteServiceLinkedRole",
            "iam:GetServiceLinkedRoleDeletionStatus"
          ]
          "Resource": "arn:aws:iam::*:role/aws-service-role/*"
        },
        # allow create log groups for the container
        {
          Effect   = "Allow"
          "Action": [
            "logs:*",
          ]
          "Resource": "*"
        },
        {
          Effect   = "Allow"
          "Action": [
            "ssm:*",
          ]
          "Resource": "*"
        },
        {
          Effect   = "Allow"
          "Action": [
            "ecr:*",
          ]
          "Resource": "*"
        },
        {
          Effect   = "Allow"
          "Action": [
            "ssm:*",
          ]
          "Resource": "*"
        },
      ]
    })
  }
}

data "aws_iam_policy" "task_execution_base_policy" {
  name = "AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.python_auth_task_role.name
  policy_arn = data.aws_iam_policy.task_execution_base_policy.arn
}

resource "aws_ecs_task_definition" "python_auth_task_definition" {
  family = "${local.prefix}_task_definition"
  # # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition
  requires_compatibilities = ["FARGATE"]
  # required for fargate
  network_mode             = "awsvpc"
  # required for fargate
  cpu                      = 512
  # required for fargate
  memory                   = 1024

  execution_role_arn = aws_iam_role.python_auth_task_role.arn

  container_definitions = jsonencode([
    {
      name      = "${local.prefix}_task_definition"
      image     = "531861524173.dkr.ecr.eu-central-1.amazonaws.com/johnny-ecr:dev-python-auth"
      cpu       = 10
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 8000
          hostPort      = 8000
        }
      ]
      environment = [
        { name = "DB_USERNAME", value = aws_db_instance.python_auth.username },
        { name = "DB_PASSWORD", value = aws_db_instance.python_auth.password },
        { name = "DB_NAME", value = aws_db_instance.python_auth.db_name },
        { name = "DB_HOST", value = aws_db_instance.python_auth.address },
        { name = "DB_PORT", value = tostring(aws_db_instance.python_auth.port) },
        # TODO: replace with correct value
        { name = "GOOGLE_CLIENT_ID", value = "" },
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
            awslogs-create-group= "true",
            awslogs-group= "${local.prefix}_task_logs",
            awslogs-region= "eu-central-1",
            awslogs-stream-prefix= "awslogs-python_auth"
        }
      }
    },
  ])
}

# Example of an AWS ECS Service using the created Task Definition
resource "aws_ecs_service" "python_auth_ecs_service" {
  name = "${local.prefix}_ecs_service"
  cluster         = aws_ecs_cluster.python_auth_cluster.id
  task_definition = aws_ecs_task_definition.python_auth_task_definition.arn
  desired_count   = 1  # Set the desired number of tasks to run
  launch_type = "FARGATE"



  # required for fargate
  network_configuration {
    subnets = data.aws_subnets.default_subnets.ids
    # required for fargate: This will allow to hit on the container from outside
    assign_public_ip = true
  }

  # Load Balancer Configuration (Optional)
  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_target_group.arn 
    # Name of the container in the task_definition
    container_name   = "${local.prefix}_task_definition"
    container_port   = 8000
  }

  # Ensure that the service starts after the task definition is created
  depends_on = [aws_ecs_task_definition.python_auth_task_definition]
}

# ========================== USING EC2 ================================================

# resource "aws_ecs_task_definition" "python_auth_task_definition" {
#   family = "${local.prefix}_task_definition"
#   # # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition
#   execution_role_arn = aws_iam_role.python_auth_task_role.arn

#   container_definitions = jsonencode([
#     {
#       name      = "${local.prefix}_task_definition"
#       image     = "531861524173.dkr.ecr.eu-central-1.amazonaws.com/johnny-ecr:dev-python-auth"
#       cpu       = 10
#       memory    = 512
#       essential = true
#       portMappings = [
#         {
#           containerPort = 8000
#           hostPort      = 8000
#         }
#       ]
#       environment = [
#         { name = "DB_USERNAME", value = aws_db_instance.python_auth.username },
#         { name = "DB_PASSWORD", value = aws_db_instance.python_auth.password },
#         { name = "DB_NAME", value = aws_db_instance.python_auth.db_name },
#         { name = "DB_HOST", value = aws_db_instance.python_auth.address },
#         { name = "DB_PORT", value = tostring(aws_db_instance.python_auth.port) },
#         # { name = "GOOGLE_ID", value = "" },
#       ]

#     },
#   ])
# }
# # Example of an AWS ECS Service using the created Task Definition
# resource "aws_ecs_service" "python_auth_ecs_service" {
#   name = "${local.prefix}_ecs_service"
#   cluster         = aws_ecs_cluster.python_auth_cluster.id
#   task_definition = aws_ecs_task_definition.python_auth_task_definition.arn
#   desired_count   = 1  # Set the desired number of tasks to run

#   # # Load Balancer Configuration (Optional)
#   # load_balancer {
#   #   target_group_arn = "arn:aws:elasticloadbalancing:us-east-1:123456789012:targetgroup/my-target-group/1234567890123456"
#   #   container_name   = "my-container"
#   #   container_port   = 80
#   # }
#   # Ensure that the service starts after the task definition is created
#   depends_on = [aws_ecs_task_definition.python_auth_task_definition]
# }

