
resource "aws_lb" "ecs_lb" {
  name               = "${local.dash_prefix}-ecs-lb"
  internal           = false
  load_balancer_type = "application"

  subnets          = data.aws_subnets.default_subnets.ids

  tags = {
    Name = "${local.dash_prefix}-ecs-lb"
  }
}

resource "aws_lb_target_group" "ecs_target_group" {
  name     = "${local.dash_prefix}-target-group"
  port     = 8000
  protocol = "HTTP"
  # Required target_type for fargate
  target_type = "ip"

  vpc_id = data.aws_vpc.default_vpc.id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 30
    matcher             = "200-299"
    timeout             = 5
    path="/docs"
  }
}


# This Route53_zone was created by AWS during the registration by hand of the domain
data "aws_route53_zone" "ecs_zone" {
  name = "${local.domain_name}"
}

# resource "aws_route53_record" "ecs_dns_record" {
#   zone_id = data.aws_route53_zone.ecs_zone.zone_id
#   name    = "${local.domain_name}"
#   type    = "A"

#   alias {
#     name                   = aws_lb.ecs_lb.dns_name
#     zone_id                = aws_lb.ecs_lb.zone_id
#     evaluate_target_health = false
#   }
# }

# Record to link SUBDOMAIN to load balancer
resource "aws_route53_record" "ecs_dns_record" {
  zone_id = data.aws_route53_zone.ecs_zone.zone_id
  name    = "${local.subdomain_name}"
  type    = "A"

  alias {
    name                   = aws_lb.ecs_lb.dns_name
    zone_id                = aws_lb.ecs_lb.zone_id
    evaluate_target_health = false
  }
}

# # We do not need www for api
# resource "aws_route53_record" "www_ecs_dns_record" {
#   zone_id = data.aws_route53_zone.ecs_zone.zone_id
#   name    = "www.${local.subdomain_name}"
#   type    = "A"

#   alias {
#     name                   = aws_lb.ecs_lb.dns_name
#     zone_id                = aws_lb.ecs_lb.zone_id
#     evaluate_target_health = false
#   }
# }

# ================ HTTP traffic ========================================
resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.ecs_lb.arn
  port              = 80
  protocol          = "HTTP"

  # default_action {
  #     type             = "forward"
  #     target_group_arn = aws_lb_target_group.ecs_target_group.arn
  # }

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener_rule" "http_listener_rule" {
  listener_arn = aws_lb_listener.http_listener.arn
  priority     = 100

  # action {
  #   type             = "forward"
  #   target_group_arn = aws_lb_target_group.ecs_target_group.arn
  # }
  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    host_header {
        values = ["${local.domain_name}"]
    }
  }
}

# ================ HTTPS traffic ========================================

# WARNING: destroying this resource will not destroy the certificate
resource "aws_acm_certificate" "ecs_https_certificate" {
  domain_name       = "*.${local.domain_name}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "certificate_validation" {
  certificate_arn         = aws_acm_certificate.ecs_https_certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.ecs_dns_record_https : record.fqdn]
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation
resource "aws_route53_record" "ecs_dns_record_https" {
  for_each = {
    for dvo in aws_acm_certificate.ecs_https_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.ecs_zone.zone_id
}

# #TODO: COMMENT THIS OR THE ABOVE ALTERNATIVE
# resource "aws_route53_record" "ecs_dns_record_https" {
#   zone_id = data.aws_route53_zone.ecs_zone.zone_id
#   name    = "${local.domain_name}"
#   type    = "A"
  
#   alias {
#     name                   = aws_lb.ecs_lb.dns_name
#     zone_id                = aws_lb.ecs_lb.zone_id
#     evaluate_target_health = true
#   }

#   # Use the ACM certificate ARN here
#   # The certificate must be validated before creating this resource
#   set_identifier = aws_acm_certificate.ecs_https_certificate.arn
# }


resource "aws_lb_listener" "https_listener" {
    load_balancer_arn = aws_lb.ecs_lb.arn
    port              = 443
    protocol          = "HTTPS"

    ssl_policy = "ELBSecurityPolicy-2016-08"
    certificate_arn = aws_acm_certificate.ecs_https_certificate.arn

    default_action {
        type             = "forward"
        target_group_arn = aws_lb_target_group.ecs_target_group.arn
    }
  depends_on = [ aws_acm_certificate.ecs_https_certificate, aws_acm_certificate_validation.certificate_validation ]
}

resource "aws_lb_listener_rule" "https_listener_rule" {
  listener_arn = aws_lb_listener.https_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_target_group.arn
  }

  condition {
    host_header {
        values = ["${local.domain_name}"]
    }
  }
}

# =================== Add DNS record to point to frontend deployed on VERCEL ===============================
resource "aws_route53_record" "frontend_record" {
  zone_id = data.aws_route53_zone.ecs_zone.zone_id
  name    = "${local.domain_name}"
  type    = "A"
  records = ["76.76.21.21"]
  ttl             = 60
}
