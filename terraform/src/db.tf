data "aws_ssm_parameter" "db_password" {
  name = var.ssm_path_db_password
}


resource "aws_db_instance" "python_auth" {
  allocated_storage    = 10
  db_name              = "python_auth"
  engine               = "postgres"
  instance_class       = "db.t3.micro"
  username             = "python_auth"
  publicly_accessible = true
  password             = data.aws_ssm_parameter.db_password.value
  skip_final_snapshot  = true
  identifier = "${var.env}-python-auth-rds"
#   final_snapshot_identifier = "python-auth-final-snapshot-identifier"
}

output "db_endpoint" {
  value = aws_db_instance.python_auth.endpoint
}
