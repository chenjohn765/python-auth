locals {
    prefix = "${var.env}_python_auth"
    dash_prefix = replace("${var.env}_python_auth", "_", "-")
    domain_name = "jcauth.com"
    subdomain_name = "api.${local.domain_name}"
}
