terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.29.0"
    }
  }

  backend "s3" {
    bucket         = "jchen-python-auth"
    key            = "terraform/python-auth/state.tfstate"
    dynamodb_table = "tfstates-locks"
    region         = "eu-central-1"
  }

  required_version = "~> 1.3"
}

locals {
  region         = "eu-central-1"
}

provider "aws" {
  region = local.region
}

module "module" {
  source = "../../src/"
  providers = {
    aws        = aws
  }

  ssm_path_db_password = "/python_auth/dev/db_password"
  env = "dev"
}
